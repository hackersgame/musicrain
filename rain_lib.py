#!/usr/bin/python3

import re
import urllib.request



def search(for_whatever):
    URL_base = 'https://soundcloud.com/search?q='
    search = "%20".join(for_whatever.split(' '))
    URL = URL_base + search
    return_data = []
    
    with urllib.request.urlopen(URL) as html_shit:
        html = html_shit.read()

    results = re.findall(r'<a href="\/.*?\/.*?">', str(html))
    found_results = False
    for result in results:
        if len(result.split('"')) >= 4:
            found_results = False
            continue
        if not found_results:
            if "search" not in result:
                found_results = True
        if found_results:
            print(result)
            return_data.append(result.split('"')[1])
    return return_data
    

#print(html)
